package priyandana.rizal.appx05

import android.app.AlertDialog
import android.content.Intent
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.PopupMenu
import android.widget.Toast
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        registerForContextMenu(txtContextMenu)

    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?
    ) {
        var mnuInflater = getMenuInflater()
        mnuInflater.inflate(R.menu.menu_context,menu)
        btnPopup.setOnClickListener(this)

    }

    override fun onContextItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemPaste -> {
                Toast.makeText(this,"Action Paste Dipilih", Toast.LENGTH_SHORT).show()
                return true
            }
            R.id.itemDelete -> {
                var snackbar = Snackbar.make(constraintLayout,"Action Delete Dipilih",Snackbar.LENGTH_SHORT)
                snackbar.show()
                return true
            }
            R.id.itemCopy -> {
                var alertBuilder = AlertDialog.Builder(this)
                alertBuilder.setTitle("Informasi").setMessage("Action Copy Dipilih")
                alertBuilder.setNeutralButton("OK",null)
                alertBuilder.show()
                return true
            }
            R.id.itemPerkalian ->{
                var perkali = Intent(this,PerkalianActivity::class.java)
                startActivity(perkali)
                return true
            }
            else ->{
                Toast.makeText(this,"Tidak ada yang dipilih",Toast.LENGTH_SHORT).show()
                return false
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.menu_options,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemPlay ->{
                Toast.makeText(this,"Playing music",Toast.LENGTH_SHORT).show()
                return true
            }
            R.id.itemPause ->{
                var snackbar = Snackbar.make(constraintLayout,"Pause the music", Snackbar.LENGTH_SHORT)
                snackbar.show()
                return true
            }
            R.id.itemGalery->{
                var intentGal = Intent()
                intentGal.setType("image/*")
                intentGal.setType(Intent.ACTION_GET_CONTENT)
                startActivity(Intent.createChooser(intentGal,"Pilih Gambar"))
            }

        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        var popMenu = PopupMenu(this,v)
        popMenu.menuInflater.inflate(R.menu.menu_popup,popMenu.menu)
        popMenu.setOnMenuItemClickListener { item ->
            when(item.itemId){
                R.id.itemDownload -> {
                    Toast.makeText(baseContext, "Download Film",Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.itemPlay -> {
                    Toast.makeText(baseContext, "Upload Film", Toast.LENGTH_SHORT).show()
                    true
                }
                R.id.itemDetik -> {
                    var webUri = "https://detik.com"
                    var intentInternet = Intent(Intent.ACTION_VIEW, Uri.parse(webUri))
                    startActivity(intentInternet)
                }
        }
            false
        }
        popMenu.show()
    }
}
