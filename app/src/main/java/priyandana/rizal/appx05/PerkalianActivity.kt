package priyandana.rizal.appx05

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_perkalian.*
import java.text.DecimalFormat

class PerkalianActivity : AppCompatActivity(), View.OnClickListener {
    var x : Double = 0.0
    var y : Double = 0.0
    var hasil : Double = 0.0

    override fun onClick(v: View?) {
        x = editText2.text.toString().toDouble()
        y = editText3.text.toString().toDouble()
        hasil = x*y
        textView2.text = DecimalFormat("#.##").format(hasil)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_perkalian)
        btnperkalian.setOnClickListener(this)

        var paket : Bundle? = intent.extras
        editText2.setText(paket?.getString("X"))
    }

    override fun finish() {
        var intent = Intent()
        intent.putExtra("hasilKali",textView2.text.toString())
        setResult(Activity.RESULT_OK, intent)

        super.finish()
    }
}